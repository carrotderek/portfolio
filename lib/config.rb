require "yaml"

module Showcase
  class Config

    DATA_DIRECTORY = "data"
    def initialize
      @config = YAML.load_file(config_file)
    end

    def project_data
      File.join(DATA_DIRECTORY, @config["project_data"])
    end

    def google_analytics
      code = @config['google_analytics']
      code if code and code.length > 3
    end

    # private
    def config_file
      File.join("./config", "showcase.yml")
    end

  end
end