require "rubygems"
require "bundler"
Bundler.require
require "./app"

map "/assets" do
  environment = Sprockets::Environment.new
  environment.append_path "assets/javascripts"
  environment.append_path "assets/stylesheets"
  environment.append_path "assets/images"
  environment.append_path "assets/docs"

  run environment
end

map "/" do
  run Carrotderek::Main.new
end