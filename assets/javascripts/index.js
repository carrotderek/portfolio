$('document').ready(function () {

	$window = $(window);

	$('#about').css('height', $window.height());
	$('#masthead').css('height', $window.height());
	$('#masthead h3 a').click(function(e) {
		e.preventDefault();
		var destination = $(this).attr('href');

		$('hgroup#hello').removeClass('in').addClass('out');

		setTimeout(function() {
			$.get(destination, function(data) {
				$('body').hide().html(data)
					.fadeIn(700);

			});
		}, 1000);
	});

	var likes = ["responsive design", "data visualization", "usable interfaces", "scalable web applications", "clean code", "CSS3", "Javascript", "jQuery", "Ruby", "PHP"],
		mastheadIterator = 1;

		$('#likes').html(likes[0]);

		setInterval(function() {
		$('#likes').fadeOut(300, function() {
			$(this).html(likes[mastheadIterator]).fadeIn(300);
		});

		mastheadIterator++;
		if (mastheadIterator == likes.length) {
			mastheadIterator = 0;
		}
	}, 2000);

	function getAge(birthdate) {
		var ONE_DAY = 60*60*24*1000.
			today = Date.now(),
			birthdate = birthdate.getTime();

		difference = Math.abs(today - birthdate);
		return Math.round(difference/ONE_DAY)/365.25;
	}

	$('#age').html(getAge(new Date(1987, 08, 14)).toPrecision(4));
	
});