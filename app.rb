require "bundler"
Bundler.require
require "sinatra"
require "sinatra/reloader"
require "sinatra/partial"
require "compass"
require File.join("./lib", "showcase")

module Carrotderek
  class Main < Sinatra::Application

    showcase = Showcase::Application.new
    configure do
      set :scss, {:style => :expanded, :debug_info => true}
      Compass.add_project_configuration(File.join(Main.root, 'config', 'compass.rb'))
    end

    configure do
      register Sinatra::Partial
      set :partial_template_engine, :slim
      enable :partial_underscores
    end

    configure :development do
      register Sinatra::Reloader
    end

    get "/" do
      @title = 'carrotderek.com'
      slim :index, :layout => :index_layout
    end

    get "/projects" do
      @title = 'Projects - carrotderek.com'
      @projects = showcase.projects
      slim :projects, :layout => :index_layout
    end

    get "/about" do
      @title = 'About - carrotderek.com'
      slim :about, :layout => :index_layout
    end

  end
end