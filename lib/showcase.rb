require "yaml"
require "ostruct"
require File.join("./lib", "config")

module Showcase
  class Application 

    attr_reader :config
    VERSION = '0.0.1'

    def initialize
      @config = Config.new
      @projects = []
      load_projects
    end

    def load_projects
      data = YAML.load_file @config.project_data

      data.each do |key, project|
        project_key = {:key => key}
        @projects << OpenStruct.new(project.merge(project_key))

        @projects.sort! {|proj1, proj2| proj1.id.to_i <=> proj2.id.to_i}
      end
    end

    def projects
        @projects
    end

  end
end